var app = angular.module('plunker', []);

app.controller('glController', function($scope) {

  var prazenRacun = function() {
    var racun = {
      name: 'Janez Novak',
      startingBalace: 100.00,
      runningBalance: 74.34
    }
    return racun;
  };
  
   var praznaTransakcija = function() {
    var tranz= {
      type: 'debit',
      amount: 0.00,
      opis: ''
    }

    return tranz;
  };

  var vseTranz = [{
    amount: 100.00,
    opis: 'polog',
    type: 'credit'
  }, {
    amount: 50.00,
    opis: 'trgovina',
    type: 'debit'
  }, {
    amount: 25.26,
    opis: 'drugo',
    type: 'debit'
  }, {
    amount: 100.00,
    opis: 'Deposit',
    type: 'credit'
  }, {
    amount: 35.15,
    opis: 'racun',
    type: 'debit'
  }, {
    amount: 16.25,
    opis: 'trgovina',
    type: 'debit'
  }, ];



  $scope.tranz = praznaTransakcija();
  $scope.racun = prazenRacun();

  $scope.tList = vseTranz;

  $scope.saveTransaction = function() {
    var amount = parseFloat($scope.tranz.amount);
    var num = parseFloat($scope.racun.runningBalance);
    var odg = 0;
    if ($scope.tranz.type === 'credit') {
      odg = num + amount
    } else {
      odg = num - amount
    }
    $scope.racun.runningBalance = odg;

    $scope.tranz.amount = amount;
    vseTranz.push($scope.tranz);
    $scope.tranz = praznaTransakcija();
  };

});


app.directive('moneywarn', function() {
  var staticWarningLevel = .2;

  return {
    restrict: 'A',
    scope: {
      val: '=moneywarn'
    },
    link: function(scope, element, attrs) {
      scope.$watch('val', function(newValue) {
        var startBalance = parseInt(attrs.startbalance);
        var warningLevel = startBalance * staticWarningLevel;
        if (newValue === warningLevel) {
          element.addClass('alert-warning');
           element.removeClass('alert-danger');
        } else if (newValue < warningLevel) {
          element.addClass('alert-danger');
        } else {
          element.removeClass('alert-warning');
          element.removeClass('alert-danger');
        }

      }, true);
    }
  }

});

